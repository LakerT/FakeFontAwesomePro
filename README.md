This is an empty placeholder for setting up [Pronouns.page](https://gitlab.com/PronounsPage/PronounsPage)
and other projects that use FontAwesome Pro without a need for a license. 

```
# Git over SSH (preferred)
yarn add git+ssh://git@gitlab.com:Avris/FakeFontAwesomePro.git

# Git over HTTPS
yarn add git+https://git@gitlab.com:Avris/FakeFontAwesomePro.git
```

